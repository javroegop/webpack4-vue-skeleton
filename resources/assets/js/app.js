import Vue from 'vue/dist/vue';
import App from '../components/App.vue';

new Vue({
    el: '#app',
    components: {
    	App
    },
    template: '<app></app>'
});
