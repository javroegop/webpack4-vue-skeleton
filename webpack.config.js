let webpack = require('webpack');
let path = require('path');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
	entry: {
		bundle: './resources/assets/js/app.js',
	},
	output: {
		path: path.resolve(__dirname, 'public/js'),
		filename: 'bundle.js',
		publicPath: '/js/'
	},
	devServer: {
		contentBase: path.resolve(__dirname, 'public'),
		publicPath: '/js/',
		port: 8080,
		host: '127.0.0.1',
		watchContentBase: true
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				loader: [
					MiniCssExtractPlugin.loader,
					'css-loader'
				]
			},
			{
				test: /\.scss$/,
				loader: [
					MiniCssExtractPlugin.loader,
					'css-loader', 
					'sass-loader'
				]
			}, 
			{
				test: /\.vue$/,
				loader: ['vue-loader']
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
      		filename: "[name].css",
      		chunkFilename: "[id].css"
    	}),
    	new VueLoaderPlugin()
	]
}
